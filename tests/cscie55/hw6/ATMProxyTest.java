package cscie55.hw6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ATMProxyTest {
	
	/** Requires that Server is running. See Overview - Unit Testing - Testing ATMProxy */
	// Close and restart the Server before running these tests
	
	/** Instantiate ATMProxy object, which would throw if connection fails
	 *  
	 * Code snippet from Client: ATM atm = new ATMProxy(host, port);
	 * given above, we want to test the ATMProxy(host, port) constructor
	 */
	@Test
	public void constructProxy() throws ATMException{
		ATMProxy atm = new ATMProxy("localhost", 7777);
		atm.hangup();
	}
	
	/** test getBalance 
	 * @throws ATMException */
	@Test
	public void getBalanceTest() throws ATMException {
		ATMProxy atm = new ATMProxy("localhost", 7777);
		assertEquals(0.0f,atm.getBalance(), 0.001f);
		atm.hangup();
	}
	
	/** test deposit
	 * Keep in mind that the same Server is running all along.
	 * @throws ATMException */
	@Test
	public void depositTest() throws ATMException {
		ATMProxy atm = new ATMProxy("localhost", 7777);
		float origBalance = atm.getBalance();
		atm.deposit(1000.0f);
		float newBalance = atm.getBalance();
		assertEquals(1000.0f, newBalance - origBalance, 0.001f);
		atm.hangup();
	}
	
	/** test withdraw
	 * Keep in mind that the same Server is running all along.
	 * @throws ATMException */
	@Test
	public void withdrawTest() throws ATMException {
		ATMProxy atm = new ATMProxy("localhost", 7777);
		atm.deposit(10001.0f);
		float origBalance = atm.getBalance(); // hanging here
		atm.withdraw(1000.0f);
		float newBalance = atm.getBalance();
		assertEquals(-1000.0f, newBalance - origBalance, 0.001f);
		atm.hangup();
	}

}
