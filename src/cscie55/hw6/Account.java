package cscie55.hw6;


public class Account {
	private float balance;
	
	public Account(float balance) throws ATMException {
		if (Float.isInfinite(balance) || Float.isNaN(balance)) {
			throw new ATMException("Impossible account balance.");
		}
		this.balance = balance;
	}
	
	protected synchronized void setBalance(float updated) throws ATMException {
		if (Float.isInfinite(updated) || Float.isNaN(updated)) {
			throw new ATMException("Impossible account balance.");
		}
		balance = updated;
	}
	
	public synchronized float getBalance() {
		return balance;
	}
}