package cscie55.hw6;

import java.io.PrintWriter;

/**
 * A work order object responsible for executing ATMMessage objects. Can be run by a thread.
 * 
 * @see ATMThread
 */

public class ATMRunnable implements Runnable {
	
	/** Our atm object, on which to execute work orders */
	private ATM atm;
	/** The incoming message that carries a work order, according to ATMProtocol */
	private ATMMessage inMsg;
	/** A socket writer to talk to client */
	private PrintWriter out;
	
	public ATMRunnable(ATM atm, ATMMessage inMsg, PrintWriter out) {
		this.atm = atm;
		this.inMsg = inMsg;
		this.out = out;
	}
	
	/**
	 * Executes on atm, sends results to client.
	 * 
	 * @throws ATMException
	 */
	public void run() {
		ATMMessage outMsg = null;
		try {
			outMsg = inMsg.execute(atm);
			if (outMsg != null) {
				Server.log("Sending String: " + outMsg.serialize());
				out.println(outMsg.serialize());
			}
		} catch (ATMException ae) {
			ae.printStackTrace();
		}

	}
}