/**
 * Server communicates with Client (through ATMProxy) and delegates to ATMImplementation.
 * 
 * @author Bret Fontecchio
 * @version 1.1
 * @since January 25, 2014
 */

package cscie55.hw6;


import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import cscie55.hw6.ATMProtocol.ATMLexicon;

public class Server {
	/*
	 * Constants
	 */
	/** Developer mode is specified in the constructor. */
	private static final boolean DEV_MODE = true;
	/** Fail-Fast causes the Server to shut down as soon as an exception occurs. */
	private static boolean FAIL_FAST;
	/** Verbosity leads to increased output. */
	private static boolean VERBOSE;
	/** Exceptions the server recovered from which are beleived to be the client's fault can be
	 *  hidden or visible. Hiding them can be convenient for debugging the server. */
	private static boolean SUPPRESS_CLIENT_ERRORS;
	
	/*
	 * Network Resources
	 */
	/** Our continously bound socket, which always listens for clients */
	private ServerSocket serverSocket = null;
	/** Our client socket, which will be closed and reopened for each client */
	private Socket clientSocket = null;
	/** socketWriter outputs to socket OutputStream in a formatted manner.
	 *  see Overview Implementation Notes Server[2] for limitations of PrintWriter.
	 */
	/*
	 * IO Resources
	 */
	/** See Implementation notes on PRINTWRITER */
	protected PrintWriter socketWriter = null;
	/** Scanner object which reads from client socket's input stream */
	private Scanner socketReader = null;
	
	/** our ATM */
	ATM atm;
	
	/** Synchronized, Runnable Message Queue */
	Queue<ATMRunnable> q; 
	
	/** Thread Pool */
	Queue<ATMThread> pool;
	
	/**
	 * Server constructor sets up ServerSocket and goes into either developer mode or 
	 * not developer (i.e. production) mode.
	 * 
	 * @throws ATMException
	 */
	public Server(int port) throws ATMException {
		this.atm = new ATMImplementation();
		try {
			this.serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
			/**
			 * See Implementation note Server.1 in Overview.txt for note on common IOException
			 */
			throw new ATMException(
					"Could not bind Server Socket."
					+ "There may be an ATM Server already running on this machine.", true);
		}
		if (DEV_MODE) {
			FAIL_FAST = false;	// This is a bit annoying, even for a developer.
			SUPPRESS_CLIENT_ERRORS = false;
			VERBOSE = true;
		} else {
			FAIL_FAST = false;
			VERBOSE = false;
			SUPPRESS_CLIENT_ERRORS = true;
		}
		q = new LinkedList<ATMRunnable>();
		pool = new LinkedList<ATMThread>();
		initPool(5);	// create thread pool
	}

	/**
	 * Go into (near) infinite loop, listening for client connections until serviceClient() 
	 * throws an exception. Conditions which break the loop should modify <code>running</code>.
	 * 
	 * @param args
	 * @throws ATMException
	 */
	public static void main(String[] args) throws ATMException {
		if (args.length == 1) {
			Server server = new Server(Integer.parseInt(args[0]));
			/** running is always true, as of v1.0 */
			boolean running = true;
			while (running) {
				try {
					// accept new client
					server.clientSocket = server.serverSocket.accept();
					log("Accepting Client");
					server.socketReader = new Scanner(server.clientSocket.getInputStream());
					server.socketWriter = new PrintWriter(server.clientSocket.getOutputStream(), true);
					// talk to client
					try {
						server.serviceClient();
					} catch (ATMException e) {
						if (!SUPPRESS_CLIENT_ERRORS) {
							System.out.println("Exception caught: " + e.getMessage());
							e.printStackTrace();
						} else {
							log("Hanging up on buggy client.");
						}
						// serviceClient();		// continuing to service the client is a possibility
					}
					// disconnect from client
					server.socketReader.close();
					server.socketWriter.close();
					server.clientSocket.close();
				}  catch (Exception e) {
					if (FAIL_FAST) {
						// fail-fast -- slam the brakes on because something isn't right
						throw new ATMException(e.getMessage());
					} else {
						// defensive -- keep the server running even though client had a problem
						if (!SUPPRESS_CLIENT_ERRORS) {
							System.out.println("Exception caught: " + e.getMessage());
							e.printStackTrace();
						} else {
							log("Exception caught. Client disconnected unexpectedly.");
						}
					} 
				}
			}
			server.cleanUpConnections();
		}
		else {
			System.out.println("Usage: Server <port>");
		}
	}
	
	/**
	 * Service loop creates runnable object with work order containing message and resources and puts in message queue.
	 * 
	 * @throws ATMException in many relatively mundane cases, such as client hanging up
	 * unexpectedly. It is up to the Server main to handle these Exceptions.
	 */
	private void serviceClient() throws ATMException {
		serviceLoop: while (true) {
			String incomingString="";
			if (socketReader.hasNext()) {		// hasNext should block if still connected
				incomingString = socketReader.nextLine();
				log("Received String: " + incomingString);
				ATMMessage inMsg = ATMProtocol.getATMMessage(incomingString);
				if (inMsg.getIdentifier() == ATMLexicon.BYE) {
					log("Client said BYE\n");
					break serviceLoop;
				} else {
					synchronized(q) {
						ATMRunnable r = new ATMRunnable(atm, inMsg, socketWriter);
						q.add(r);
						q.notifyAll();
					}
				}
			} else {
				log("Client hung up without saying BYE.\n");
				break serviceLoop;
			}
		}
	}
	
	/**
	 * Initialize thread pool by creating threads, storing them in the top level pool queue, and starting them.
	 * 
	 * @param numThreads the number of threads to create and start
	 */
	private void initPool(int numThreads){
		for (int i = 0; i < numThreads; i++) {
			ATMThread t = new ATMThread(q, i);
			t.start();
			pool.add(t);
		}
	}
	
	/**
	 * Timestamp a message and decide whether or not to send it to stdout, depending on whether
	 * we're in <code>VERBOSE</code> mode. This is a good place to add file logging, although a
	 * shell redirect would work well for most admins.
	 * 
	 * @param msg The message to log
	 */
 	protected static void log(String msg) {
 		String time = (new Timestamp(System.currentTimeMillis())).toString();
		msg = String.format("%s\t%s", time, msg);
 		if (VERBOSE) {
 			System.out.println(msg);
 		}
 	}
	
 	/**
 	 * Attempts to close all sockets. Try-catch-finally blocks nested to make sure server
 	 * socket is closed even if client socket can't be closed.
 	 * 
 	 * @throws ATMException
 	 */
	private void cleanUpConnections() throws ATMException {
		if(serverSocket != null) {
			if(clientSocket != null) {
				try {
					clientSocket.close();
				} catch (IOException eio) {
					throw new ATMException ("Couldn't close client socket.", true);
				} finally {
					try {
						serverSocket.close();
					} catch (Exception e) {
						throw new ATMException("Couldn't close server socket.", true);
					}
				}
			}
		}
	}
}
