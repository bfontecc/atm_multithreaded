/**
 * Retrieves ATMRunnabe objects from request queue and runs them. Synchronizes on request queue to avoid race conditions amongst
 * threads. 
 * 
 * @author Bret Fontecchio
 */

package cscie55.hw6;

import java.util.Queue;

public class ATMThread extends Thread {
	
	Queue<ATMRunnable> q;
	
	/** this thread's id */
	int id;
	
	/**
	 * ATMThread Constructor
	 * @param q the work order queue
	 * @param id this thread's id
	 */
	public ATMThread(Queue<ATMRunnable> q, int id) {
		this.q = q;
	}
	
	/** Run loop is the thread's entry point when started. This thread continually waits for a new Runnable item in the queue and 
	 * starts it up if so
	 */
	public void run() {
		while(true) {
			synchronized(q) {
				while(q.isEmpty()){
					try {
						q.wait();
					} catch (InterruptedException ie) {
						Server.log("Thread had an Exception while waiting");
					}
				}
				Server.log("Running request in thread " + id);
				ATMRunnable r = q.remove();
				r.run();
			}
		}
	}
}