RUNNING:
kill all processes related to this project
kill anything running on the port you wish to use
java -cp <jar_name.jar> cscie55.hw6.Server <port> &
<might have to hit enter again>
java -cp <jar_name.jar> cscie55.hw6.Client localhost <port>

Ampersand is to have it run in the background in Unix. I'm not sure how to do this in windows.
	
DESIGN DECISIONS:
	[1] You can overdraw from an account, but not repeatedly.
 The Account.java withdraw method has logic such that one may overdraw, but not if already overdrawn. I
 chose not to include a check for insufficient funds because I beleived that this was realistic, and in
 accordance with real banks. Later versions, however, which have a spec calling for a check here, do in
 fact have that check.

	[2] The Client waits for ACK before sending another request.
The threading will be more genuinely useful for later versions which accept multiple clients at once, 
but for now the client does one thing at a time. For this reason, thread0 is usually the one to pick up 
the request. I am planning on creating a totally multithreaded version where everything is done in 
parallel, and if Ria and/or Charlie are interested, I would be glad to show it off when possible. I think 
the choice to keep the Client and Server acknowledging is important at this stage.

IMPLEMENTATION DETAILS:
	[1] Queue's of Threads
The threads stay put in the thread pull, but I beleive a queue is useful for extensibility.
	[2] LinkedList as Queue Implementation
The threadpool queue and runnable queue are both implemented as LinkedLists. I beleive the polymorphism
makes it easy to swap out data structures at a low level, while guiding their usage with Queue.
---

Overview.txt from hw4

PROTOCOL
	[1] Use of BigDecimal
		I decided to use BigDecimal because:
		1. It lends itself to being stored as a String, which is suitable for sockets.
		2. Rounding and rescaling operations are highly reliable, explicit, and convenient.
		3. It can store very large and precise values.
		4. It's well-documented.
		5. It has its own functions for math operations.
		6. I trust its exceptions more than I trust a float to be NaN etc.
		On the other hand, it is much slower and more memory intensive than a float being converted to a 
		string and vice versa. If I thought, for example, that I would be handling thousands of 
		transactions per second on one 2013 laptop I would deprecate my use of BigDecimal.
	ATM uses default character encodings. This is for convenience during testing. I would not send a Java
	product to production with default character encodings.
SERVER
	[1] Exception While constructing Server
		It is quite common to get the error message: “Could not bind Server Socket. There may be an ATM 
		Server already running on this machine.” In most cases the assumed cause will be correct. The 
		other process can be killed as follows:
	Unix:
		lsof -w -n -i tcp:8889 | sed -E 's/[ ]+/$/g' | cut -f2 -d$ | tail -n1 | xargs kill
	Eclipse:
		hit the black 'X' “remove launch” button in the console window. Try run again and be sure to
		terminate before running the next time.
	Other than that:
		You're looking for a process using the default port (8889) and you want to kill it in order to
		free the port. If it is not another instance of Server, try setting the default server port to a
		different value.
UNIT TESTING
	[1] Testing ATMProxy
		The Server program must be run manually to do unit testing on ATMProxy. It should be restarted so
		it is not still carrying a different balance. This could be improved by mocking the Server.

SOCKET PERMISSIONS
	JDK v7.0 update51 (JDK 7u51), includes the following in the release notes:
	"""
	The default socket permissions assigned to all code including untrusted code have been changed in 
	this release. Previously, all code was able to bind any socket type to any port number greater than 
	or equal to 1024. It is still possible to bind sockets to the ephemeral port range on each system. 
	The exact range of ephemeral ports varies from one operating system to another, but it is typically 
	in the high range (such as from 49152 to 65535). The new restriction is that binding sockets outside
	 of the ephemeral range now requires an explicit permission in the system security policy.
	Most applications using client tcp sockets and a security manager will not see any problem, as these
	typically bind to ephemeral ports anyway. Applications using datagram sockets or server tcp sockets 
	(and a security manager) may encounter security exceptions where none were seen before. If this 
	occurs, users should review whether the port number being requested is expected, and if this is the 
	case, a socket permission grant can be added to the local security policy, to resolve the issue.
	"""
	---Long Story Short---:
	Since this varies system to system, I have chosen not to choose a hard wired port and give it 
	explicit security permissions. I have chosen to require the user to specify a port, as per the spec.
	I recommend a high port range, such as 50000 to 60000.
	AccessControlException may be raised if the 7u51+ system does not allow the port chosen.
TOP LEVEL CLASS FILES:
	I have included .class files at the top level in the project jar instead of just in the bin folder
	because this is the only way to make the jar runnable for multiple classes (as opposed to a specific
	entry point in the manifest). This makes the jar more cluttered to read, but easy to run. I chose 
	this tradeoff which optimizes for easy running.
